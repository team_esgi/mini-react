import checkType from '../src/TypeChecker.js'
var assert = require('assert');

describe("TypeChecker tests", () => {

    it('can check Strings', () => {
        let canCheckString = checkType("test", {
            type: "string"
        })

        assert.ok(canCheckString)
    });

    it('can check Number', () => {
        let canCheckNumber = checkType(2, {
            type: "number"
        })

        assert.ok(canCheckNumber)
    });

    it('can check Boolean', () => {
        let canCheckBoolean = checkType(false, {
            type: "boolean"
        })

        assert.ok(canCheckBoolean)
    });

    it('can check Array', () => {
        let canCheckArray = checkType([1,2,3], {
            type: "array",
            propertiesType: {
                type:"number"
            }
        })

        assert.ok(canCheckArray)
    });

    it('fails checking String when undefined', () => {
        let failsCheckingStringWhenUndefined = checkType(undefined, {
            type: "string"
        })

        assert.ok(!failsCheckingStringWhenUndefined)
    });

    it('fails checking Number when undefined', () => {
        let failsCheckingNumberWhenUndefined = checkType(undefined, {
            type: "number"
        })

        assert.ok(!failsCheckingNumberWhenUndefined)
    });

    it('fails checking Boolean when undefined', () => {
        let failsCheckingBooleanWhenUndefined = checkType(undefined, {
            type: "boolean"
        })

        assert.ok(!failsCheckingBooleanWhenUndefined)
    });

    it('fails checking Array when undefined', () => {
        let failsCheckingArrayWhenUndefined = checkType(undefined, {
            type: "array",
            propertiesType: {
                type:"number"
            }
        })

        assert.ok(!failsCheckingArrayWhenUndefined)
    });

    it('fails checking Object when undefined', () => {
        let failsCheckingObjectWhenUndefined = checkType(undefined, {
            type: "object",
            properties: {
                test: "test"
            }
        })

        assert.ok(!failsCheckingObjectWhenUndefined)
    });

    it('fails checking String when null', () => {
        let failsCheckingStringWhenNull = checkType(null, {
            type: "string"
        })

        assert.ok(!failsCheckingStringWhenNull)
    });


    it('fails checking Number when null', () => {
        let failsCheckingNumberWhenNull = checkType(null, {
            type: "number"
        })

        assert.ok(!failsCheckingNumberWhenNull)
    });

    it('fails checking Boolean when null', () => {
        let failsCheckingBooleanWhenNull = checkType(null, {
            type: "boolean"
        })

        assert.ok(!failsCheckingBooleanWhenNull)
    });

    it('fails checking Array when null', () => {
        let failsCheckingArrayWhenNull = checkType(null, {
            type: "array",
            propertiesType: {
                type:"number"
            }
        })

        assert.ok(!failsCheckingArrayWhenNull)
    });

    it('fails checking Object when null', () => {
        let failsCheckingObjectWhenNull = checkType(null, {
            type: "object",
            properties: {
                test: "test"
            }
        })

        assert.ok(!failsCheckingObjectWhenNull)
    });

    it('can check a simple object', () => {
        let mySimpleObject = {
            ar: [1,2,3],
            str: "test",
            num: 98,
            boo: false
        }

        let canCheckSimpleObject = checkType(mySimpleObject, {
            type: "object",
            properties: {
                ar: {
                    type: "array",
                    propertiesType: {
                        type: "number"
                    }
                },
                str: {
                    type: "string"
                },
                num: {
                    type: "number"
                },
                boo: {
                    type: "boolean"
                }
            }
        });

        assert.ok(canCheckSimpleObject)
    });

    it('can check a complex object', () => {
        let myRecursiveObject = {
            ar: [1,2,3],
            str: "test",
            num: 98,
            boo: false,
            obj: {
                t: "test2",
                obj2: {
                    b: true,
                    obj3: {
                        a: [1]
                    }
                }
            }
        }

        let canCheckComplexObject = checkType(myRecursiveObject, {
            type: "object",
            properties: {
                ar: {
                    type: "array",
                    propertiesType: {
                        type: "number"
                    }
                },
                str: {
                    type: "string"
                },
                num: {
                    type: "number"
                },
                boo: {
                    type: "boolean"
                },
                obj: {
                    type: "object",
                    properties: {
                        t: {
                            type: "string"
                        },
                        obj2: {
                            type: "object",
                            properties: {
                                b: {
                                    type:"boolean"
                                },
                                obj3: {
                                    type:"object",
                                    properties: {
                                        a: {
                                            type: "array",
                                            propertiesType: {
                                                type: "number"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

        assert.ok(canCheckComplexObject)
    });

    it('can check Array items type', () => {
        let myArray = [
            {
                fullName: "test",
                lastName: "test"
            },
            {
                fullName: "test",
                lastName: "test"
            },
            {
                fullName: "test",
                lastName: "test"
            }
        ]

        let canCheckArrayItemsType = checkType(myArray, {
            type: "array",
            propertiesType: {
                type: "object",
                properties: {
                    fullName: {
                        type: "string"
                    },
                    lastName: {
                        type: "string"
                    }
                }
            }
        });

        assert.ok(canCheckArrayItemsType);
    });
});


