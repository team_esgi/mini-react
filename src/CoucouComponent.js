import Component from './Component.js';

export default class CoucouComponent extends Component {

    template = `<h1 class="teal-text center">Coucou {{ fullName.firstName }} {{ fullName.lastName }}</h1>`;

    propertiesConfiguration = {
        type: "object",
        properties: {
            fullName: {
                type: "object",
                properties: {
                    firstName: {
                        type: "string"
                    },
                    lastName: {
                        type: "string"
                    }
                }
            }
        }
    }

}