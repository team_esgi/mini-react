import Component from "./Component.js";
import DepartementsList from "./departements/DepartementsListComponent.js";

export default class AppComponent extends Component {

    propertiesConfiguration = {
        type: "object",
        properties: {
            fullName: {
                type: "object",
                properties: {
                    firstName: {
                        type: "string"
                    },
                    lastName: {
                        type: "string"
                    }
                }
            }
        }
    }

    getDepartements() {
        return fetch("https://geo.api.gouv.fr/departements")
            .then(response => response.json())
    }

    render() {
        this.template = "<h1 class='teal-text center'>GeoApp</h1>";
        return this.getDepartements()
            .then(data => {
                return new DepartementsList().display(data);
            })
            .then((departementsListTemplate) => {
                this.template += departementsListTemplate
            })
    }

}