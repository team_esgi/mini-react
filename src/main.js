import CoucouComponent from './CoucouComponent.js';
import UserListComponent from './users/UserListComponent.js';
import AppComponent from "./AppComponent.js";
import Router from './Router.js'

let root = document.getElementById('root')

const router = new Router();


router.get('/', () => {
    let appComponent = new AppComponent();
    appComponent.display(null)
        .then((template) => {
            root.innerHTML = template;
        })
});

router.get('/users-list', () => {
    console.log("ah");
    let users = [
        {
            firstName: "Julien",
            lastName: "Legoanvic"
        },
        {
            firstName: "Samuel",
            lastName: "Benitah"
        },
        {
            firstName: "Alix",
            lastName: "de Haut"
        }
    ];

    let userList = new UserListComponent()
    userList.display(users)
        .then((template) => {
            root.innerHTML = template;
        })
})

router.get('/coucou', () =>{
    let user =  {
        fullName: {
            firstName: "Monsieur",
            lastName: "Karl"
        }
    }
    let coucouComponent = new CoucouComponent()
    coucouComponent.display(user)
        .then((template) => {
            root.innerHTML = template;
        })
})

router.init()



