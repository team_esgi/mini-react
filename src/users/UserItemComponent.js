import Component from '../Component.js';

export default class UserItemComponent extends Component {

    template = "<li class='collection-item'> {{firstName}} {{lastName}} </li>";

    propertiesConfiguration = {
        type: "object",
        properties: {
            firstName: {
                type: "string"
            },
            lastName: {
                type: "string"
            }
        }
    }

}