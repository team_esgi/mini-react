import Component from '../Component.js';
import UserItemComponent from './UserItemComponent.js';

export default class UserListComponent extends Component {

    propertiesConfiguration = {
        type: "array",
        propertiesType: {
            type: "object",
            properties: {
                firstName: {
                    type: "string"
                },
                lastName: {
                    type: "string"
                }
            }
        }
    }

    render() {
        this.template = "<h1 class='teal-text'>Liste des utilisateurs</h1><ul class='collection'>";

        let promise = Promise.resolve();
        for(let i = 0; i < this.props.length; i++) {
            promise = promise.then(() => {
                return new UserItemComponent().display(this.props[i]);
            }).then((item) => {
                this.template += item;
            });
        }

        return promise.then(() => {
            this.template += "</ul>"
        })
    }

}